import React, { lazy } from 'react'
import { Route, Switch } from 'react-router-dom'

const ProposalCreate = lazy(() => import('./modules/proposal-create/components/ProposalCreate'))
const ProposalEdit = lazy(() => import('./modules/proposal-edit/components/ProposalEdit'))
const ProposalIndex = lazy(() => import('./modules/proposal-index/components/ProposalIndex'))
const NotFound = lazy(() => import('./components/NotFound'))

const Routes = () => (
  <Switch>
    <Route exact path="/proposal/create">
      <ProposalCreate />
    </Route>
    <Route exact path="/proposal/update">
      <ProposalEdit />
    </Route>
    <Route exact path="/proposal/index">
      <ProposalIndex />
    </Route>
    <Route path="*">
      <NotFound />
    </Route>
  </Switch>
)

export default Routes