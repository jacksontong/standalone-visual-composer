export interface Proposal {
  id: number
  name: string
  created_at: number
  updated_at: number
  pages: number[]
}

export interface ProposalPage {
  id: number
  name: string
  content: string
  order: number
  created_at: number
  updated_at: number
}

export interface ProposalResponse {
  data: Proposal[]
  meta: {
    [key: string]: string
  },
  links: {
    self: string
    first?: string
    prev?: string
    next?: string
    last?: string
  }
}