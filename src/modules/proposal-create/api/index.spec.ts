import axios from 'axios'
import { createProposal } from './'

const mockedAxios = axios as jest.Mocked<typeof axios>;
jest.mock('axios')

describe('create proposal api', () => {
  afterEach(() => {
    mockedAxios.get.mockRestore()
  })

  it('call api with correct url and param', async () => {
    const name = "test proposal"

    mockedAxios.post.mockImplementationOnce(() => Promise.resolve({}))

    await createProposal(name)

    await expect(mockedAxios.post).toHaveBeenCalledWith(
      `/open-solar/proposal/create`,
      { name }
    )
  })

  it('return proposal on success', async () => {
    const name = "test proposal"
    mockedAxios.post.mockImplementationOnce(() => Promise.resolve({
      name
    }))

    await expect(createProposal(name)).resolves.toEqual({ name })
  })
})