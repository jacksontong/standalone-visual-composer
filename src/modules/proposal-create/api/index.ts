import axios, { AxiosResponse } from "axios"
import { Proposal } from "../../../types/proposal"

export const createProposal = async (name: String): Promise<AxiosResponse<Proposal>> => {
  const res = await axios.post(`/open-solar/proposal/create`, { name })

  return res
}