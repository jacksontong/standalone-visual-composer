import React from 'react'
import { Helmet } from "react-helmet"
import * as Yup from "yup"
import { createProposal } from '../api'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import ProposalForm from '../../../components/form/ProposalForm'
import Breadcrumb from './Breadcrumb'

const ProposalCreate = () => {
  const history = useHistory()

  return (
    <div className="container-fluid new-proposal">
      <Helmet>
        <title>Create New Proposal</title>
      </Helmet>
      <Breadcrumb />
      <h1>Create New Proposal</h1>

      <ProposalForm
        initialValues={{
          name: ''
        }}
        validationSchema={Yup.object({
          name: Yup.string()
            .required()
            .min(6)
            .max(255)
        })}
        onSubmit={async ({ name }, { setSubmitting }) => {
          try {
            const res = await createProposal(name)

            toast.success('Proposal has been created!')
            setSubmitting(false)

            history.push(`/proposal/update?id=${res.data.id}`)
          } catch (e) {
            toast.error(e.message)
            if (e.response.data) {
              toast.error(JSON.stringify(e.response.data))
            }
            setSubmitting(false)
          }
        }}
      />
    </div>
  )
}

export default ProposalCreate