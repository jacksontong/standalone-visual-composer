import React from 'react'
import { Link } from 'react-router-dom'

const Breadcrumb = () => (
  <ol className="breadcrumb">
    <li>
      <Link to="/proposal/index">
        Proposals
      </Link>
    </li>
    <li className="active">
      Create
    </li>
  </ol>
)

export default Breadcrumb