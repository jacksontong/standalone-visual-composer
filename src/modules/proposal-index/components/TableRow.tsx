import React from 'react'
import { Proposal } from '../../../types/proposal'
import { fromUnixTime, format } from 'date-fns'
import TableRowActions from './TableRowActions'
import Slider, { LazyLoadTypes } from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import PreviewImage from './PreviewImage'

interface Props {
  proposal: Proposal
}

const sliderSettings = {
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  lazyLoad: "ondemand" as LazyLoadTypes,
  draggable: false
}

/**
 * Beware of using React.memo on export
 */
const TableRow: React.FC<Props> = ({
  proposal: {
    name, updated_at, id, pages
  }
}) => {
  return (
    <tr>
      <td>
        {pages.length > 0 &&
          <div style={{ width: 75, margin: "0 auto" }}>
            <Slider {...sliderSettings}>
              {pages.map(page => <PreviewImage key={page} page={page} />)}
            </Slider>
          </div>
        }
      </td>
      <td>
        {name}
      </td>
      <td>{pages.length}</td>
      <td>
        {format(fromUnixTime(updated_at), "d LLL. yyyy")}
      </td>
      <td>
        <TableRowActions id={id} />
      </td>
    </tr>
  )
}

export default React.memo(TableRow)