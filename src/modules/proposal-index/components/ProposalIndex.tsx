import React, { useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilePdf, faPlusSquare } from '@fortawesome/free-solid-svg-icons'
import Helmet from 'react-helmet'
import { Link } from 'react-router-dom'
import { DynamicModuleLoader } from 'redux-dynamic-modules'
import { ProposalIndexModule } from '../ProposalIndexModule'
import ProposalTable from './ProposalTable'
import { useDispatch } from 'react-redux'
import { ProposalActions } from '../actions'
import { useQuery } from '../../../hooks'

const ProposalIndex = () => {
  const dispatch = useDispatch()
  const query = useQuery()
  useEffect(() => {
    dispatch(ProposalActions.loadProposals(query))
  }, [query, dispatch])
  return (
    <div className="container-fluid">
      <Helmet>
        <title>Proposals</title>
      </Helmet>

      <Link
        to="/proposal/create"
        className="btn tpl-btn-primary btn-lg pull-right"
      >
        <FontAwesomeIcon icon={faPlusSquare} /> New Proposal
      </Link>

      <h1>
        <FontAwesomeIcon icon={faFilePdf} /> Proposal
      </h1>

      <ProposalTable />
    </div>
  )
}

export default () => (
  <DynamicModuleLoader modules={[ProposalIndexModule]}>
    <ProposalIndex />
  </DynamicModuleLoader>
)