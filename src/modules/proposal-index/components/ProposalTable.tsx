import React from 'react'
import { useSelector } from 'react-redux'
import { getVisibleProposals, getPagination } from '../selectors/proposals'
import TableRow from './TableRow'
import Pagination from '../../../components/pagination/Pagination'

const ProposalTable = () => {
  const proposals = useSelector(getVisibleProposals)
  const { pageCount, page } = useSelector(getPagination)

  if (!proposals.length) {
    return <span />
  }

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Preview Image</th>
            <th>Name</th>
            <th>Num Pages</th>
            <th>Updated At</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {proposals.map(proposal =>
            <TableRow key={proposal.id} proposal={proposal} />
          )}
        </tbody>
      </table>
      <Pagination
        pageCount={pageCount}
        currentPage={page}
        baseUrl={window.location.href}
      />
    </div>
  )
}

export default ProposalTable