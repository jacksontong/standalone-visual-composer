import React from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { useDispatch } from 'react-redux'
import { ProposalActions } from '../actions'

interface Props {
  id: number
}

const TableRowActions: React.FC<Props> = ({
  id
}) => {
  const dispatch = useDispatch()

  const handleDelete = async () => {
    if (window.confirm("Are you sure you want to delete this item?")) {
      dispatch(ProposalActions.requestDeleteProposal(id))
    }
  }
  return (
    <ul className="list-inline">
      <li>
        <Link title="Edit" to={`/proposal/update?id=${id}`}>
          <FontAwesomeIcon icon={faPencilAlt} />
        </Link>
      </li>
      <li>
        <button
          className="btn-primary-outline"
          onClick={handleDelete}
        >
          <FontAwesomeIcon icon={faTrashAlt} />
        </button>
      </li>
    </ul>
  )
}

export default TableRowActions