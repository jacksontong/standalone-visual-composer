import React from 'react'
import { OverlayTrigger, Popover } from 'react-bootstrap'

interface Props {
  page: number
}
const PreviewImage: React.FC<Props> = ({
  page
}) => {
  const popover = (
    <Popover id="popover-trigger-hover-focus">
      <img
        src="https://via.placeholder.com/225x300"
        alt={page.toString()}
      />
    </Popover>
  )
  return (
    <OverlayTrigger
      key={page}
      trigger={['hover', 'focus']}
      overlay={popover}
    >
      <img
        src="https://via.placeholder.com/75x100"
        alt={page.toString()}
      />
    </OverlayTrigger>
  )
}

export default PreviewImage