import { ISagaModule } from "redux-dynamic-modules-saga";
import { IProposalIndexAwareState } from "./types/contracts"
import rootReducer from './reducers'
import rootSaga from './sagas'

export const ProposalIndexModule: ISagaModule<IProposalIndexAwareState> = {
  id: "proposalIndex",
  reducerMap: {
    proposalIndexState: rootReducer
  } as any,
  sagas: [rootSaga]
}