import axios, { AxiosResponse } from "axios"
import { ProposalResponse } from "../../../types/proposal"

export const getProposals = async (
  searchParams: URLSearchParams = new URLSearchParams()
): Promise<AxiosResponse<ProposalResponse>> => {
  const searchParam = searchParams.toString()
  return await axios.get('/open-solar/proposal/index' + (searchParam !== "" ? `?${searchParam}` : ''))
}

export const deleteProposal = async (id: number) => {
  return await axios.delete(`/open-solar/proposal/delete?id=${id}`)
}