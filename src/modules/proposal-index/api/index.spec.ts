import axios from 'axios'
import { getProposals, deleteProposal } from './'

const mockedAxios = axios as jest.Mocked<typeof axios>;
jest.mock('axios')

describe('proposal index api', () => {
  describe('deleteProposal', () => {
    afterEach(() => {
      mockedAxios.delete.mockRestore()
    })

    it('call api with correct url', async () => {
      mockedAxios.delete.mockImplementationOnce(() => Promise.resolve({}))
      const id = 3

      await deleteProposal(id)

      expect(mockedAxios.delete).toHaveBeenCalledWith(
        `/open-solar/proposal/delete?id=${id}`
      )
    })
  })
  describe('getProposals', () => {
    afterEach(() => {
      mockedAxios.get.mockRestore()
    })
    it('call api with correct url', async () => {
      mockedAxios.get.mockImplementationOnce(() => Promise.resolve({}))

      await getProposals()

      expect(mockedAxios.get).toHaveBeenCalledWith(
        '/open-solar/proposal/index'
      )
    })

    it('resolve data as promise', async () => {
      const res = {
        data: [
          {
            id: 5,
            name: "test new proposal",
            created_by: 1195,
            created_at: 1577513785,
            updated_at: 1577513785
          },
          {
            id: 6,
            name: "another new proposal",
            created_by: 1195,
            created_at: 1577513852,
            updated_at: 1577513852
          },
        ]
      }
      mockedAxios.get.mockImplementationOnce(() => Promise.resolve(res))

      await expect(getProposals()).resolves.toEqual(res)
    })

    it('cans select another page in pagination', async () => {
      const res = {
        data: [
          {
            id: 5,
            name: "test new proposal",
            created_by: 1195,
            created_at: 1577513785,
            updated_at: 1577513785
          },
          {
            id: 6,
            name: "another new proposal",
            created_by: 1195,
            created_at: 1577513852,
            updated_at: 1577513852
          },
          {
            id: 7,
            name: "jackson tong",
            created_by: 1195,
            created_at: 1577513896,
            updated_at: 1577513896
          },
          {
            id: 8,
            name: "Jackson Tong",
            created_by: 1195,
            created_at: 1577514442,
            updated_at: 1577518683
          }
        ],
        meta: {
          pageSize: 4,
          pageCount: 2,
          page: 1
        },
        links: {
          self: "/standalone/proposal/update?page=2&per-page=4",
          first: "/standalone/proposal/update?page=1&per-page=4",
          prev: "/standalone/proposal/update?page=1&per-page=4"
        }
      }
      const page = '2'

      mockedAxios.get.mockImplementationOnce(() => Promise.resolve(res))

      await getProposals(new URLSearchParams({
        page
      }))

      expect(mockedAxios.get).toHaveBeenCalledWith(
        `/open-solar/proposal/index?page=${page}`
      )
    })
  })
})