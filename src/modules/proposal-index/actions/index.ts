import { createAction, ActionsUnion } from "../../../utils/ActionHelper"
import { ProposalsSchema } from "../types/schema"
import { Pagination } from "../types/contracts"

export enum ProposalActionTypes {
  LOAD_PROPOSAL = "ProposalsIndex/LOAD",
  RECEIVED_PROPOSAL = "ProposalsIndex/RECEIVED_PROPOSAL",
  RECEIVED_PAGINATION = "ProposalsIndex/RECEIVED_PAGINATION",
  REQUEST_DELETE_PROPOSAL = "ProposalsIndex/REQUEST_DELETE_PROPOSAL",
  PROPOSAL_DELETED = "ProposalsIndex/PROPOSAL_DELETED"
}

export const ProposalActions = {
  loadProposals: (searchParams: URLSearchParams = new URLSearchParams({})) => createAction(ProposalActionTypes.LOAD_PROPOSAL, { searchParams }),
  receivedProposals: (response: ProposalsSchema) =>
    createAction(ProposalActionTypes.RECEIVED_PROPOSAL, { response }),
  receivedPagination: (response: Pagination) =>
    createAction(ProposalActionTypes.RECEIVED_PAGINATION, { response }),
  requestDeleteProposal: (id: number) => createAction(ProposalActionTypes.REQUEST_DELETE_PROPOSAL, { id }),
  proposalDeleted: (id: number) => createAction(ProposalActionTypes.PROPOSAL_DELETED, { id })
}

export type ProposalActionsUnion = ActionsUnion<typeof ProposalActions>