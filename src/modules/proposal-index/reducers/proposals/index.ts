import { combineReducers } from "redux"
import allIds from './allIds'
import byId from './byId'
import { ProposalReducer } from "../../types/contracts"
import { ProposalActionsUnion } from "../../actions"

const proposals = combineReducers<ProposalReducer, ProposalActionsUnion>({
  allIds,
  byId
})

export default proposals