import allIds from './allIds'
import { ProposalActions } from '../../actions'

describe('proposal allIds reducer', () => {
  it('should handle RECEIVED_PROPOSAL action', () => {
    expect(allIds([19], ProposalActions.receivedProposals({
      entities: {
        proposals: {
          '20': {
            id: 20,
            name: 'magento',
            created_at: 1577623075,
            updated_at: 1577682471,
            pages: [
              1,
              2,
            ]
          },
          '23': {
            id: 23,
            name: 'how about you',
            created_at: 1577623075,
            updated_at: 1577623075,
            pages: [
              9,
              12
            ]
          },
        }
      },
      result: [
        20,
        23,
      ]
    }
    ))).toEqual([20, 23])
  })

  it('should handle PROPOSAL_DELETED', () => {
    expect(allIds([20, 23], ProposalActions.proposalDeleted(23))).toEqual([20])
  })
})