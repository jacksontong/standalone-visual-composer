import { ProposalById } from "../../types/contracts"
import { ProposalActionsUnion, ProposalActionTypes } from "../../actions"
import produce from 'immer'

const initialState = {}

const byId = (
  state: ProposalById = initialState,
  action: ProposalActionsUnion
): ProposalById =>
  produce(state, draft => {
    switch (action.type) {
      case ProposalActionTypes.RECEIVED_PROPOSAL:
        return action.payload.response.entities.proposals
      case ProposalActionTypes.PROPOSAL_DELETED:
        delete draft[action.payload.id]
        break
    }
  })

export default byId