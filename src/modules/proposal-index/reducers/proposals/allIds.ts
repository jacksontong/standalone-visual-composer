import { ProposalAllIds } from "../../types/contracts"
import { ProposalActionsUnion, ProposalActionTypes } from "../../actions"
import produce from 'immer'

const initialState = [] as ProposalAllIds

const allIds = (
  state: ProposalAllIds = initialState,
  action: ProposalActionsUnion
): ProposalAllIds =>
  produce(state, draft => {
    switch (action.type) {
      case ProposalActionTypes.RECEIVED_PROPOSAL:
        return action.payload.response.result
      case ProposalActionTypes.PROPOSAL_DELETED:
        return draft.filter(id => id !== action.payload.id)
    }
  })

export default allIds