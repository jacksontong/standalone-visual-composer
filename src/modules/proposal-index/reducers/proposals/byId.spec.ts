import byId from './byId'
import { ProposalActions } from '../../actions'

describe('proposal byId reducer', () => {
  it('should handle RECEIVED_PROPOSAL action', () => {
    expect(byId({
      '19': {
        id: 19,
        name: 'magento',
        created_at: 1577623075,
        updated_at: 1577682471,
        pages: [
          1,
          2,
        ]
      },
    }, ProposalActions.receivedProposals({
      entities: {
        proposals: {
          '20': {
            id: 20,
            name: 'magento',
            created_at: 1577623075,
            updated_at: 1577682471,
            pages: [
              1,
              2,
            ]
          },
          '23': {
            id: 23,
            name: 'how about you',
            created_at: 1577623075,
            updated_at: 1577623075,
            pages: [
              9,
              12
            ]
          },
        }
      },
      result: [
        20,
        23,
      ]
    }
    ))).toEqual({
      '20': {
        id: 20,
        name: 'magento',
        created_at: 1577623075,
        updated_at: 1577682471,
        pages: [
          1,
          2,
        ]
      },
      '23': {
        id: 23,
        name: 'how about you',
        created_at: 1577623075,
        updated_at: 1577623075,
        pages: [
          9,
          12
        ]
      },
    })
  })

  it('should handle PROPOSAL_DELETED', () => {
    expect(byId({
      '20': {
        id: 20,
        name: 'magento',
        created_at: 1577623075,
        updated_at: 1577682471,
        pages: [
          1,
          2,
        ]
      },
      '23': {
        id: 23,
        name: 'how about you',
        created_at: 1577623075,
        updated_at: 1577623075,
        pages: [
          9,
          12
        ]
      },
    }, ProposalActions.proposalDeleted(23))).toEqual({
      '20': {
        id: 20,
        name: 'magento',
        created_at: 1577623075,
        updated_at: 1577682471,
        pages: [
          1,
          2,
        ]
      },
    })
  })
})