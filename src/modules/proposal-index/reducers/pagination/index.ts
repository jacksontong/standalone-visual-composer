import { Pagination } from "../../types/contracts"
import { ProposalActionsUnion, ProposalActionTypes } from "../../actions"

const initialState = {} as Pagination

const pagination = (
  state: Pagination = initialState,
  action: ProposalActionsUnion
): Pagination => {
  switch (action.type) {
    case ProposalActionTypes.RECEIVED_PAGINATION:
      return action.payload.response
    default:
      return state;
  }
}

export default pagination