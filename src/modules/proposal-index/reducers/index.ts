import proposals from './proposals'
import pagination from './pagination'
import { combineReducers } from 'redux'

export default combineReducers({
  proposals,
  pagination
})