import { all, takeLatest, fork, call, put, takeEvery } from "@redux-saga/core/effects"
import { ProposalActionTypes, ProposalActions } from "../actions"
import { getProposals, deleteProposal } from "../api"
import { normalize } from "normalizr"
import { ProposalSchema, ProposalsSchema } from "../types/schema"
import { toast } from "react-toastify"

function* handleRequestProposals(action: any) {
  const response = yield call(getProposals, action.payload.searchParams)
  const normalizedData: ProposalsSchema = normalize(response.data.data, [ProposalSchema])

  yield put(ProposalActions.receivedProposals(normalizedData))
  yield put(ProposalActions.receivedPagination(response.data.meta))
}

function* handleRequestDeleteProposal(action: ReturnType<typeof ProposalActions.requestDeleteProposal>) {
  const id = action.payload.id

  try {
    yield call(deleteProposal, id)

    yield call(toast.success, "Proposal has been deleted!")
    yield put(ProposalActions.proposalDeleted(id))
  } catch (e) {
    yield call(toast.error, "Error while deleting page")
  }
}

function* watchRequestDeleteProposal() {
  yield takeEvery(ProposalActionTypes.REQUEST_DELETE_PROPOSAL, handleRequestDeleteProposal)
}

function* watchRequestProposals() {
  yield takeLatest(ProposalActionTypes.LOAD_PROPOSAL, handleRequestProposals)
}

export default function* root() {
  yield all([
    fork(watchRequestProposals),
    fork(watchRequestDeleteProposal),
  ])
}