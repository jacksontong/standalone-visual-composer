import { schema, NormalizedSchema } from "normalizr"
import { StringMap } from "../../../utils/ActionHelper"
import { Proposal, ProposalPage } from "../../../types/proposal"

export type ProposalsSchema = NormalizedSchema<{
  proposals: StringMap<Proposal>
}, number[]>

export type PagesSchema = NormalizedSchema<{
  pages: StringMap<ProposalPage>
}, number[]>

export type PageSchema = NormalizedSchema<{
  pages: StringMap<ProposalPage>
}, number>

export const PageSchema = new schema.Entity('pages')

export const ProposalSchema = new schema.Entity('proposals', {
  pages: [PageSchema]
})