import { StringMap, ActionWithPayload } from "../../../utils/ActionHelper"
import { Proposal } from "../../../types/proposal"

export type ProposalById = StringMap<Proposal>

export type ProposalAllIds = number[]

export interface ProposalReducer {
  allIds: ProposalAllIds
  byId: ProposalById
}

export interface IProposalIndexState {
  proposals: ProposalReducer
  pagination: Pagination
}

export interface IProposalIndexAwareState {
  proposalIndexState: IProposalIndexState
}

export interface Pagination {
  pageSize: number
  pageCount: number
  page: number
}