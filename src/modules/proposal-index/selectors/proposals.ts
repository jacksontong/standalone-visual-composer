import { IProposalIndexAwareState } from "../types/contracts";
import { createSelector } from "reselect";

export const getAllIds = ({ proposalIndexState: state }: IProposalIndexAwareState) => state.proposals.allIds

export const getById = ({ proposalIndexState: state }: IProposalIndexAwareState) => state.proposals.byId

export const getVisibleProposals = createSelector(
  [
    getAllIds,
    getById
  ],
  (allIds, byId) => allIds.map(id => byId[id])
)

export const getPagination = ({ proposalIndexState: state }: IProposalIndexAwareState) => state.pagination