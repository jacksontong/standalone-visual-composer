
import { IProposalEditAwareState } from "../types/contracts";

export const getActivePage = ({ proposalEditState: state }: IProposalEditAwareState) => state.root.activePage