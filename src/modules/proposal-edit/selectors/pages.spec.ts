import { IProposalEditAwareState } from "../types/contracts"
import { getOrderedPages } from "./pages"


describe('pages selector', () => {
  it('getOrderedPages order the pages by order attribute', () => {
    const state: IProposalEditAwareState = {
      proposalEditState: {
        root: { activePage: null },
        pages: {
          allIds: [10, 11, 12, 13],
          byId: {
            "10": {
              id: 10,
              name: "page 10",
              order: 0,
              content: '',
              created_at: 1234,
              updated_at: 1234
            },
            "11": {
              id: 11,
              name: "page 11",
              order: 2,
              content: '',
              created_at: 1234,
              updated_at: 1234
            },
            "12": {
              id: 12,
              name: "page 12",
              order: 1,
              content: '',
              created_at: 1234,
              updated_at: 1234
            },
            "13": {
              id: 13,
              name: "page 13",
              order: 3,
              content: '',
              created_at: 1234,
              updated_at: 1234
            }
          }
        }
      }
    }
    expect(getOrderedPages(state)).toEqual([
      {
        id: 10,
        name: "page 10",
        order: 0,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      {
        id: 12,
        name: "page 12",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      {
        id: 11,
        name: "page 11",
        order: 2,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      {
        id: 13,
        name: "page 13",
        order: 3,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    ])
  })
})