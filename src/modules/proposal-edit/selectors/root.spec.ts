import { IProposalEditAwareState } from "../types/contracts"
import { getActivePage } from './root'

describe("proposal edit root selector", () => {
  it('get active page', () => {
    let state: IProposalEditAwareState = {
      proposalEditState: {
        pages: {
          byId: {},
          allIds: []
        },
        root: {
          activePage: null
        }
      }
    }
    expect(getActivePage(state)).toEqual(null)

    state.proposalEditState.root.activePage = 3
    expect(getActivePage(state)).toEqual(3)
  })
})