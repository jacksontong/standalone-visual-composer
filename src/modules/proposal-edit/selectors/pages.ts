import { IProposalEditAwareState } from "../types/contracts"
import { createSelector } from "reselect"

export const getAllIds = ({
  proposalEditState: state
}: IProposalEditAwareState) => state.pages.allIds

export const getById = ({
  proposalEditState: state
}: IProposalEditAwareState) => state.pages.byId

export const getOrderedPages = createSelector(
  [
    getAllIds,
    getById
  ],
  (allIds, byId) => allIds.map(id => byId[id])
    .sort((a, b) => a.order > b.order ? 1 : -1)
)