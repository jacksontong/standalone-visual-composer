import React, { useState, lazy, Suspense } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBook, faPlusSquare } from '@fortawesome/free-solid-svg-icons'
import PageCarousel from './carousel/PageCarousel'
import Fallback from '../../../components/Fallback'
const AsyncNewPageModal = lazy(() => import('./NewPageModal'))

const PageManage = () => {
  const [modalShown, setModalShown] = useState(false)
  const handleModalClose = () => {
    setModalShown(false)
  }
  return (
    <div>
      <h3 className="tpl-panel-title no-picture">
        <FontAwesomeIcon icon={faBook} /> Edit Pages
      </h3>
      <div className="tpl-panel">
        <div>
          <p>
            <button
              className="btn tpl-btn-primary btn-lg"
              onClick={() => {
                setModalShown(true)
              }}
            >
              <FontAwesomeIcon icon={faPlusSquare} /> New Page
            </button>
          </p>
          {modalShown &&
            <Suspense fallback={<Fallback />}>
              <AsyncNewPageModal
                modalShown={modalShown}
                handleClose={handleModalClose}
              />
            </Suspense>
          }
        </div>
        <PageCarousel />
      </div>
    </div>
  )
}

export default PageManage