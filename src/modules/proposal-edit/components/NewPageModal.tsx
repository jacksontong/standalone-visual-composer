import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import { Formik, Form } from 'formik'
import InputGroupField from '../../../components/form/InputGroupField'
import SubmitButton from '../../../components/form/SubmitButton'
import { createPage } from '../api'
import { toast } from 'react-toastify'
import { useQuery } from '../../../hooks'
import * as Yup from 'yup'
import { useDispatch } from 'react-redux'
import { ProposalEditActions } from '../actions'
import { normalize } from 'normalizr'
import { PageSchema } from '../../proposal-index/types/schema'

interface Props {
  modalShown: boolean
  handleClose: () => void
}
const NewPageModal: React.FC<Props> = ({
  modalShown,
  handleClose
}) => {
  const query = useQuery()
  const dispatch = useDispatch()

  const proposalId = query.get('id')

  return (
    <Modal
      show={modalShown}
      onHide={handleClose}
    >
      <Modal.Header closeButton>
        <Modal.Title>New Page</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Formik
          initialValues={{
            name: ''
          }}
          validationSchema={Yup.object({
            name: Yup.string()
              .required()
              .min(6)
              .max(255)
          })}
          onSubmit={async ({ name }, { setSubmitting, resetForm }) => {
            try {
              const response = await createPage({
                name,
                proposal_id: Number(proposalId)
              })
              dispatch(ProposalEditActions.addedPage(
                normalize(response.data, PageSchema)
              ))

              resetForm()
              toast.success('New Page has been created.')
              setSubmitting(false)
            } catch (error) {
              toast.error(error.message)
              if (error.response.data) {
                toast.error(JSON.stringify(error.response.data))
              }
              setSubmitting(false)
            }
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <InputGroupField
                name="name"
                label="Name"
              />
              <SubmitButton isSubmitting={isSubmitting}>
                Save
                </SubmitButton>
            </Form>
          )}
        </Formik>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleClose}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default NewPageModal