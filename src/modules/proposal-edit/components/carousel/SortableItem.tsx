import React from 'react'
import { SortableElement } from "react-sortable-hoc"
import { ProposalPage } from "../../../../types/proposal"
import "./SortableItem.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { useDispatch } from 'react-redux'
import { ProposalEditActions } from '../../actions'
import _ from 'lodash'

interface Props extends ProposalPage {
  onEditClick?: () => void
  isActivePage: boolean
}

/**
 * Beware of using React.memo on export
 */
const SortableItem = SortableElement(({
  name,
  id,
  onEditClick,
  isActivePage,
  order
}: Props) => {
  const dispatch = useDispatch()
  const onClickDelete = () => {
    if (window.confirm(`Are you sure you want to delete ${name}. This cannot be reversed.`)) {
      dispatch(ProposalEditActions.requestDeletePage(id))
    }
  }
  return (
    <div
      className={`sortable-item ${isActivePage ? 'active' : ''}`}
      onClick={onEditClick}
    >
      <h3 className="title">
        {id} - {name}
      </h3>
      <div className="actions">
        <button
          title="Edit Page"
          className="btn-primary-outline"
          onClick={onEditClick}
        >
          <FontAwesomeIcon icon={faPencilAlt} />
        </button>
        <button
          onClick={onClickDelete}
          title="Delete Page"
          className="btn-primary-outline"
        >
          <FontAwesomeIcon icon={faTrashAlt} />
        </button>
      </div>
      <p className="page-num">{order + 1}</p>
      <img src="https://via.placeholder.com/140x190" />
    </div>
  )
})

export default React.memo(SortableItem, (prevProps, nextProps) =>
  _.isEqual(
    _.omit(prevProps, 'onEditClick'),
    _.omit(nextProps, 'onEditClick')
  )
)