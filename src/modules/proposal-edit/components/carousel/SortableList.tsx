import React from 'react'
import { SortableContainer } from 'react-sortable-hoc'
import { ProposalPage } from '../../../../types/proposal'
import SortableItem from './SortableItem'
import "./SortableList.css"
import { useSelector, useDispatch } from 'react-redux'
import { getActivePage } from '../../selectors/root'
import { ProposalEditActions } from '../../actions'

interface Props {
  pages: ProposalPage[]
}

const SortableList = SortableContainer(({ pages }: Props) => {
  const activePage = useSelector(getActivePage)
  const dispatch = useDispatch()
  return (
    <div
      className="sortable-list"
    >
      {pages.map((page, index) => (
        <SortableItem
          {...page}
          isActivePage={page.id === activePage}
          key={index}
          index={index}
          onEditClick={() => {
            dispatch(
              ProposalEditActions.setActivePage(page.id)
            );
          }}
        />
      ))}
    </div>
  )
})

export default SortableList