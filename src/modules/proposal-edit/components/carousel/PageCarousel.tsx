import React, { useState, useEffect } from 'react'
import { SortEndHandler } from "react-sortable-hoc"
import update from "immutability-helper"
import SortableList from './SortableList'
import { useSelector, useDispatch } from 'react-redux'
import { getOrderedPages } from '../../selectors/pages'
import { ProposalPage } from '../../../../types/proposal'
import { ProposalEditActions } from '../../actions'

const PageCarousel = () => {
  const sortedPages = useSelector(getOrderedPages)
  const dispatch = useDispatch()
  const [pages, setPages] = useState<ProposalPage[]>([])
  useEffect(() => {
    setPages(sortedPages)
  }, [sortedPages])

  const onSortEnd: SortEndHandler = ({ oldIndex, newIndex }) => {
    const dragPage = pages[oldIndex]
    const newSortedPages = update(pages, {
      $splice: [[oldIndex, 1], [newIndex, 0, dragPage]]
    })
    setPages(newSortedPages)
    dispatch(ProposalEditActions.savePagesOrder(newSortedPages))
  }
  return (
    <SortableList
      pages={pages}
      axis="x"
      // onSortStart={(_, event) => event.preventDefault()}
      onSortEnd={onSortEnd}
      pressDelay={130}
    />
  )
}

export default PageCarousel