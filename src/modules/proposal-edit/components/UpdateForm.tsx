import React, { useState } from 'react'
import ProposalForm from '../../../components/form/ProposalForm'
import { toast } from 'react-toastify'
import * as Yup from 'yup'
import { Proposal } from '../../../types/proposal'
import { updateProposal } from '../api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp, faAngleDown, faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { Collapse } from 'react-bootstrap'

interface Props {
  proposal: Proposal,
  setProposal: React.Dispatch<React.SetStateAction<Proposal | undefined>>
}

const UpdateForm: React.FC<Props> = ({
  proposal,
  setProposal
}) => {
  const [isOpen, setIsOpen] = useState(true)

  return (
    <div className="add-bottom-md">
      <h3 className="tpl-panel-title no-picture">
        <FontAwesomeIcon icon={faInfoCircle} /> Proposal Details
        <button
          className="pull-right"
          onClick={() => setIsOpen(!isOpen)}
        >
          <FontAwesomeIcon
            icon={isOpen ? faAngleUp : faAngleDown}
            color="black"
          />
        </button>
      </h3>
      <Collapse in={isOpen}>
        <div className="tpl-panel">
          <ProposalForm
            initialValues={{
              name: proposal.name
            }}
            validationSchema={Yup.object({
              name: Yup.string()
                .required()
                .min(6)
                .max(255)
            })}
            onSubmit={async ({ name }, { setSubmitting }) => {
              try {
                const { data } = await updateProposal(proposal.id, name)

                setProposal(data)
                setSubmitting(false)
                toast.success('Proposal has been updated successfully!')
              } catch (error) {
                setSubmitting(false)
                toast.error(error.message)
                if (error.response.data) {
                  toast.error(JSON.stringify(error.response.data))
                }
              }
            }}
          />
        </div>
      </Collapse>
    </div>
  )
}

export default UpdateForm