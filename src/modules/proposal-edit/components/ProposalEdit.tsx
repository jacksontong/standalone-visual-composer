import React, { useEffect, useState } from 'react'
import Helmet from 'react-helmet'
import { useQuery } from '../../../hooks'
import { useHistory } from 'react-router-dom'
import { Proposal } from '../../../types/proposal'
import { getProposal } from '../api'
import UpdateForm from './UpdateForm'
import PageManage from './PageManage'
import Breadcrumb from './Breadcrumb'
import { DynamicModuleLoader } from 'redux-dynamic-modules'
import { ProposalEditModule } from '../ProposalEditModule'
import { useDispatch } from 'react-redux'
import { normalize } from 'normalizr'
import { PageSchema, PagesSchema } from '../../proposal-index/types/schema'
import { ProposalEditActions } from '../actions'

const ProposalEdit = () => {
  const query = useQuery()
  const history = useHistory()
  const proposalId = query.get('id')
  const [proposal, setProposal] = useState<Proposal>()
  const dispatch = useDispatch()

  if (!proposalId) {
    history.push('/not-found')
  }

  // only trigger when proposal id changed
  useEffect(() => {
    (async () => {
      try {
        const { data: proposal } = await getProposal(Number(proposalId))
        const normalizedPages: PagesSchema = normalize(proposal.pages, [PageSchema])

        setProposal(proposal)
        dispatch(ProposalEditActions.receivedPages(normalizedPages))
      } catch (e) {
        if (e.response.status === 404) {
          history.push('/not-found')
        }
      }
    })()
  }, [proposalId, history, dispatch])

  return (
    <div className="container-fluid">
      <Helmet>
        <title>Edit Proposal</title>
      </Helmet>
      <Breadcrumb />
      <h1>Edit Proposal</h1>

      {proposal &&
        <UpdateForm
          proposal={proposal}
          setProposal={setProposal}
        />
      }

      <PageManage />
    </div>
  )
}

export default () => (
  <DynamicModuleLoader modules={[ProposalEditModule]}>
    <ProposalEdit />
  </DynamicModuleLoader>
)