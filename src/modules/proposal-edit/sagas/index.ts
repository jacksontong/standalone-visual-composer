import { ProposalPage } from "../../../types/proposal"
import { all, takeLatest, fork, call, put, takeEvery } from "@redux-saga/core/effects"
import { ProposalEditActionTypes, ProposalEditActions } from "../actions"
import { savePagesOrder, deletePage } from "../api"
import { toast } from "react-toastify"
import { normalize } from "normalizr"
import { PageSchema } from "../../proposal-index/types/schema"

function* handleSavePagesOrder(action: any) {
  const pages: ProposalPage[] = action.payload.pages
  try {
    const response = yield call(savePagesOrder, pages)
    yield put(ProposalEditActions.receivedPagesAttributes(
      normalize(response.data, [PageSchema])
    ))
  } catch (error) {
    yield call(toast.error, "Error while saving page's order")
  }
}

function* handleRequestDeletePage(action: ReturnType<typeof ProposalEditActions.requestDeletePage>) {
  try {
    const { pageId } = action.payload
    yield call(deletePage, pageId)
    yield put(ProposalEditActions.deletedPage(pageId))
    yield call(toast.success, "The page has been deleted.")
  } catch (error) {
    yield call(toast.error, "Error while deleting page")
  }
}

function* watchRequestDeletePage() {
  yield takeEvery(ProposalEditActionTypes.REQUEST_DELETE_PAGE, handleRequestDeletePage)
}

function* watchSavePagesOrder() {
  yield takeLatest(ProposalEditActionTypes.SAVE_PAGES_ORDER, handleSavePagesOrder)
}

export default function* root() {
  yield all([
    fork(watchSavePagesOrder),
    fork(watchRequestDeletePage)
  ])
}