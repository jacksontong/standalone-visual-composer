import allIds from './allIds'
import { ProposalEditActions } from '../../actions'

describe("page allIds reducer", () => {
  it('should handle RECEIVED_PAGES action', () => {
    expect(allIds([19], ProposalEditActions.receivedPages({
      result: [20, 21],
      entities: {
        pages: {}
      }
    }))).toEqual([20, 21])
  })
  it('should handle DELETED_PAGE action', () => {
    expect(allIds([19, 20, 21], ProposalEditActions.deletedPage(20))).toEqual([19, 21])
  })
  it('should handle ADDED_PAGE action', () => {
    expect(allIds([], ProposalEditActions.addedPage({
      result: 12,
      entities: {
        pages: {}
      }
    }))).toEqual([12])

    expect(allIds([12], ProposalEditActions.addedPage({
      result: 13,
      entities: {
        pages: {}
      }
    }))).toEqual([12, 13])
  })
})