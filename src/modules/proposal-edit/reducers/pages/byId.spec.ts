import byId from './byId'
import { ProposalEditActions } from '../../actions'

describe('page byId reducer', () => {
  it('should handle DELETED_PAGE action', () => {
    expect(byId({
      "19": {
        id: 19,
        name: "page 19",
        order: 0,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "20": {
        id: 20,
        name: "page 20",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "21": {
        id: 21,
        name: "page 21",
        order: 2,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    }, ProposalEditActions.deletedPage(21))).toEqual({
      "19": {
        id: 19,
        name: "page 19",
        order: 0,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "20": {
        id: 20,
        name: "page 20",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    })
  })
  it('should handle RECEIVED_PAGES_ATTRIBUTES', () => {
    expect(byId({
      "19": {
        id: 19,
        name: "page 19",
        order: 0,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "20": {
        id: 20,
        name: "page 20",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "21": {
        id: 21,
        name: "page 21",
        order: 2,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    }, ProposalEditActions.receivedPagesAttributes({
      entities: {
        pages: {
          "19": {
            id: 19,
            name: "page 19",
            order: 1,
            content: '',
            created_at: 1234,
            updated_at: 1234
          },
          "20": {
            id: 20,
            name: "page 20",
            order: 0,
            content: '',
            created_at: 1234,
            updated_at: 1234
          },
          "21": {
            id: 21,
            name: "page 21",
            order: 2,
            content: '',
            created_at: 1234,
            updated_at: 1234
          }
        }
      },
      result: []
    }))).toEqual({
      "19": {
        id: 19,
        name: "page 19",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "20": {
        id: 20,
        name: "page 20",
        order: 0,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "21": {
        id: 21,
        name: "page 21",
        order: 2,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    })
  })
  it('should handle ADDED_PAGE action', () => {
    expect(byId({}, ProposalEditActions.addedPage({
      entities: {
        pages: {
          "19": {
            id: 19,
            name: "page 19",
            order: 1,
            content: '',
            created_at: 1234,
            updated_at: 1234
          }
        }
      },
      result: 19
    }))).toEqual({
      "19": {
        id: 19,
        name: "page 19",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    })

    expect(byId({
      "18": {
        id: 19,
        name: "page 18",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    }, ProposalEditActions.addedPage({
      entities: {
        pages: {
          "19": {
            id: 19,
            name: "page 19",
            order: 1,
            content: '',
            created_at: 1234,
            updated_at: 1234
          }
        }
      },
      result: 19
    }))).toEqual({
      "18": {
        id: 19,
        name: "page 18",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "19": {
        id: 19,
        name: "page 19",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    })
  })
  it('should hanle RECEIVED_PAGES action', () => {
    expect(byId({
      "19": {
        id: 19,
        name: "page 19",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    }, ProposalEditActions.receivedPages({
      entities: {
        pages: {
          "20": {
            id: 20,
            name: "page 20",
            order: 1,
            content: '',
            created_at: 1234,
            updated_at: 1234
          },
          "21": {
            id: 21,
            name: "page 21",
            order: 1,
            content: '',
            created_at: 1234,
            updated_at: 1234
          }
        }
      },
      result: [20, 21]
    }))).toEqual({
      "20": {
        id: 20,
        name: "page 20",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      },
      "21": {
        id: 21,
        name: "page 21",
        order: 1,
        content: '',
        created_at: 1234,
        updated_at: 1234
      }
    })
  })
})