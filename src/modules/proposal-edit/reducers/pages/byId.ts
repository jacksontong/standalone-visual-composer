import produce from 'immer'
import { PageById } from '../../types/contracts'
import { ProposalEditActionUnion, ProposalEditActionTypes } from '../../actions'

const initialState = {}

const byId = (
  state: PageById = initialState,
  action: ProposalEditActionUnion
): PageById =>
  produce(state, draft => {
    switch (action.type) {
      case ProposalEditActionTypes.RECEIVED_PAGES:
        return action.payload.response.entities.pages
      case ProposalEditActionTypes.ADDED_PAGE:
        const { result, entities: { pages } } = action.payload.response
        draft[result] = pages[result]
        break
      case ProposalEditActionTypes.RECEIVED_PAGES_ATTRIBUTES:
        const newPages = action.payload.response.entities.pages
        Object.keys(newPages).forEach(pageId => {
          if (state.hasOwnProperty(pageId)) {
            draft[pageId] = {
              ...draft[pageId],
              ...newPages[pageId]
            }
          }
        })
        break
      case ProposalEditActionTypes.DELETED_PAGE:
        delete draft[action.payload.pageId]
        break
    }
  })


export default byId