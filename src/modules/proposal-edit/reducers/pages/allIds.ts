import produce from 'immer'
import { PageAllIds } from '../../types/contracts'
import { ProposalEditActionUnion, ProposalEditActionTypes } from '../../actions'

const initialState = [] as PageAllIds

const allIds = (
  state: PageAllIds = initialState,
  action: ProposalEditActionUnion
): PageAllIds =>
  produce(state, draft => {
    switch (action.type) {
      case ProposalEditActionTypes.RECEIVED_PAGES:
        return action.payload.response.result
      case ProposalEditActionTypes.ADDED_PAGE:
        draft.push(action.payload.response.result)
        break
      case ProposalEditActionTypes.DELETED_PAGE:
        return draft.filter(id => id !== action.payload.pageId)
    }
  })

export default allIds