import byId from './byId'
import allIds from './allIds'
import { combineReducers } from 'redux'

export default combineReducers({
  byId,
  allIds
})