import pages from './pages'
import { combineReducers } from 'redux'
import root from './root'

export default combineReducers({
  pages,
  root
})