import { ProposalEditActionUnion, ProposalEditActionTypes } from "../../actions"
import produce from "immer"
import { RootState } from "../../types/contracts"

const initialState = {
  activePage: null
}

const root = (
  state: RootState = initialState,
  action: ProposalEditActionUnion
): RootState =>
  produce(state, draft => {
    switch (action.type) {
      case ProposalEditActionTypes.SET_ACTIVE_PAGE:
        draft["activePage"] = action.payload.pageId
        break
    }
  })

export default root