import root from "."
import { ProposalEditActions } from "../../actions"

describe('proposal edit root reducer', () => {
  it('handle SET_ACTIVE_PAGE', () => {
    expect(root({
      activePage: null
    }, ProposalEditActions.setActivePage(1))).toEqual({
      activePage: 1
    })

    expect(root({
      activePage: 3
    }, ProposalEditActions.setActivePage(1))).toEqual({
      activePage: 1
    })
  })
})