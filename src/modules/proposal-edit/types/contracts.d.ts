import { StringMap } from "../../../utils/ActionHelper";
import { ProposalPage } from "../../../types/proposal";

export type PageById = StringMap<ProposalPage>

export type PageAllIds = number[]

export interface RootState {
  activePage: number | null
}

export interface PageReducer {
  allIds: PageAllIds
  byId: PageById
}

export interface IProposalEditState {
  pages: PageReducer
  root: RootState
}

export interface IProposalEditAwareState {
  proposalEditState: IProposalEditState
}