import { createAction, ActionsUnion } from "../../../utils/ActionHelper"
import { PagesSchema, PageSchema } from "../../proposal-index/types/schema"
import { ProposalPage } from "../../../types/proposal"

export enum ProposalEditActionTypes {
  RECEIVED_PAGES = "ProposalsEdit/RECEIVED_PAGES",
  ADDED_PAGE = "ProposalsEdit/ADDED_PAGE",
  SAVE_PAGES_ORDER = "ProposalsEdit/SAVE_PAGES_ORDER",
  RECEIVED_PAGES_ATTRIBUTES = "ProposalsEdit/RECEIVED_PAGES_ATTRIBUTES",
  REQUEST_DELETE_PAGE = "ProposalsEdit/REQUEST_DELETE_PAGE",
  DELETED_PAGE = "ProposalsEdit/DELETED_PAGE",
  SET_ACTIVE_PAGE = "ProposalsEdit/SET_ACTIVE_PAGE"
}

export const ProposalEditActions = {
  receivedPages: (response: PagesSchema) =>
    createAction(ProposalEditActionTypes.RECEIVED_PAGES, { response }),
  addedPage: (response: PageSchema) =>
    createAction(ProposalEditActionTypes.ADDED_PAGE, { response }),
  savePagesOrder: (pages: ProposalPage[]) =>
    createAction(ProposalEditActionTypes.SAVE_PAGES_ORDER, { pages }),
  receivedPagesAttributes: (response: PagesSchema) =>
    createAction(ProposalEditActionTypes.RECEIVED_PAGES_ATTRIBUTES, { response }),
  requestDeletePage: (pageId: number) =>
    createAction(ProposalEditActionTypes.REQUEST_DELETE_PAGE, { pageId }),
  deletedPage: (pageId: number) =>
    createAction(ProposalEditActionTypes.DELETED_PAGE, { pageId }),
  setActivePage: (pageId: number) =>
    createAction(ProposalEditActionTypes.SET_ACTIVE_PAGE, { pageId }),
}

export type ProposalEditActionUnion = ActionsUnion<typeof ProposalEditActions>