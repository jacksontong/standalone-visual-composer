import axios from 'axios'
import { getProposal, updateProposal, createPage, savePagesOrder, deletePage } from './'

const mockedAxios = axios as jest.Mocked<typeof axios>;
jest.mock('axios')

describe('edit proposal', () => {
  describe('delete page', () => {
    afterEach(() => {
      mockedAxios.delete.mockRestore()
    })

    it('call delete api with correct url', async () => {
      mockedAxios.delete.mockImplementationOnce(() => Promise.resolve({}))

      await deletePage(3)

      expect(mockedAxios.delete).toHaveBeenCalledWith(
        `/open-solar/proposal-page/delete?id=3`
      )
    })
  })
  describe('put save order', () => {
    afterEach(() => {
      mockedAxios.put.mockRestore()
    })

    it('call put api with correct url', async () => {
      mockedAxios.put.mockImplementationOnce(() => Promise.resolve({}))
      const putData = [
        {
          id: 1,
          name: '1',
          content: '',
          order: 1,
          created_at: 123,
          updated_at: 123
        }
      ]

      await savePagesOrder(putData)

      expect(mockedAxios.put).toHaveBeenCalledWith(
        '/open-solar/proposal-page/save-order',
        putData
      )
    })

    it('resolve response', async () => {
      const putData = [
        {
          id: 1,
          name: '1',
          content: '',
          order: 1,
          created_at: 123,
          updated_at: 123
        }
      ]
      mockedAxios.put.mockImplementationOnce(() => Promise.resolve(putData))

      await expect(savePagesOrder(putData)).resolves.toEqual(putData)
    })
  })
  describe('post page', () => {
    afterEach(() => {
      mockedAxios.post.mockRestore()
    })

    it('call post api with correct url', async () => {
      mockedAxios.post.mockImplementationOnce(() => Promise.resolve({}))
      const postData = {
        name: 'test',
        proposal_id: 3
      }

      await createPage(postData)

      expect(mockedAxios.post).toHaveBeenLastCalledWith(
        `/open-solar/proposal-page/create`,
        postData
      )
    })

    it('resolve response', async () => {
      const data = {
        name: 'test',
        proposal_id: 3
      }
      mockedAxios.post.mockImplementationOnce(() => Promise.resolve(data))

      await expect(createPage(data)).resolves.toEqual(data)
    })
  })
  describe('get proposal', () => {

    afterEach(() => {
      mockedAxios.get.mockRestore()
    })

    it('call get api with correct url', async () => {
      mockedAxios.get.mockImplementationOnce(() => Promise.resolve({}))
      const id = 3

      await getProposal(id)

      expect(mockedAxios.get).toHaveBeenCalledWith(
        `/open-solar/proposal/view?id=${id}`
      )
    })

    it('return data', async () => {
      const data = {
        name: 'test'
      }
      mockedAxios.get.mockImplementationOnce(() => Promise.resolve(data))
      const id = 3

      await expect(getProposal(id)).resolves.toEqual(data)
    })
  })

  describe('update proposal', () => {
    afterEach(() => {
      mockedAxios.put.mockRestore()
    })

    it('call update api with correct url', async () => {
      const name = 'new name'
      mockedAxios.put.mockImplementationOnce(() => Promise.resolve({ name }))
      const id = 3

      await updateProposal(id, name)

      expect(mockedAxios.put).toHaveBeenCalledWith(
        `/open-solar/proposal/update?id=${id}`,
        { name }
      )
    })

    it('return data on success', async () => {
      const data = { name: 'test' }
      mockedAxios.put.mockImplementationOnce(() => Promise.resolve(data))
      const id = 3

      await expect(updateProposal(id, 'test')).resolves.toEqual(data)
    })
  })
})