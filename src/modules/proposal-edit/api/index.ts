import axios, { AxiosResponse } from 'axios'
import { Proposal, ProposalPage } from '../../../types/proposal'

export const getProposal = async (id: number): Promise<AxiosResponse<Proposal>> => {
  return await axios.get(`/open-solar/proposal/view?id=${id}`)
}

export const updateProposal = async (id: number, name: string): Promise<AxiosResponse<Proposal>> => {
  return await axios.put(`/open-solar/proposal/update?id=${id}`, {
    name
  })
}

interface CreatePagePost {
  name: string
  proposal_id: number
}
export const createPage = async (postData: CreatePagePost) => {
  return await axios.post(`/open-solar/proposal-page/create`, postData)
}

export const savePagesOrder = async (putData: ProposalPage[]) => {
  return await axios.put('/open-solar/proposal-page/save-order', putData)
}

export const deletePage = async (id: number) => {
  await axios.delete(`/open-solar/proposal-page/delete?id=${id}`)
}