import { ISagaModule } from "redux-dynamic-modules-saga"
import { IProposalEditAwareState } from "./types/contracts"
import rootReducer from './reducers'
import rootSaga from './sagas'

export const ProposalEditModule: ISagaModule<IProposalEditAwareState> = {
  id: "proposalEdit",
  reducerMap: {
    proposalEditState: rootReducer
  } as any,
  sagas: [rootSaga]
}