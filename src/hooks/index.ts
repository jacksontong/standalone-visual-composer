import { useLocation } from "react-router-dom"

/**
 * https://reacttraining.com/react-router/web/example/query-parameters
 */
export function useQuery() {
  return new URLSearchParams(useLocation().search);
}