import React from 'react'
import { Alert } from 'react-bootstrap'

const NotFound = () => (
  <div className="container">
    <h1>Not Found (#404)</h1>
    <Alert bsStyle="danger">
      <p>The requested page does not exist.</p>
    </Alert>
    <p>The above error occurred while the Web server was processing your request.</p>
    <p>Please contact us if you think this is a server error. Thank you.</p>
  </div>
)

export default NotFound