import React from 'react'
import { Formik, FormikValues, Form, FormikConfig } from 'formik'
import InputGroupField from './InputGroupField'
import SubmitButton from './SubmitButton'

interface Values extends FormikValues {
  name: string
}

const ProposalForm: React.FC<FormikConfig<Values>> = (props) => (
  <Formik
    {...props}
  >
    {({ isSubmitting }) => (
      <Form>
        <InputGroupField
          name="name"
          label="Name"
        />
        <SubmitButton isSubmitting={isSubmitting}>
          Submit
        </SubmitButton>
      </Form>
    )}
  </Formik>
)

export default ProposalForm