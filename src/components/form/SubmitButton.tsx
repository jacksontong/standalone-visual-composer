import React from 'react'
import { Button, ButtonProps } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

interface Props extends ButtonProps {
  isSubmitting: boolean
}

const SubmitButton: React.FC<Props> = ({
  isSubmitting,
  children,
  bsStyle = "primary",
  bsSize = "large",
  ...rest
}) => (
    <Button
      bsStyle={bsStyle}
      bsSize={bsSize}
      disabled={isSubmitting}
      type="submit"
      {...rest}
    >
      {isSubmitting ? <FontAwesomeIcon icon={faSpinner} spin /> : children}
    </Button>
  )

export default SubmitButton