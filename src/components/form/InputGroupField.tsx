import React from 'react'
import { useField, FieldAttributes } from 'formik'
import { FormGroup, FormControl, ControlLabel, HelpBlock } from "react-bootstrap"
import { FieldMetaProps } from "formik"

const getValidationState = (meta: FieldMetaProps<any>) => {
  if (meta.touched) {
    return meta.error ? "error" : "success"
  }

  return null
}

const InputGroupField = ({ label, type, ...props }: FieldAttributes<any>) => {
  const [field, meta] = useField(props);
  const hasError = meta.touched && meta.error;

  return (
    <FormGroup
      controlId={props.id}
      bsSize="large"
      validationState={getValidationState(meta)}
    >
      <ControlLabel>{label}</ControlLabel>
      <FormControl
        type={type}
        {...field}
        {...props}
      />
      <FormControl.Feedback />
      <HelpBlock>{hasError ? meta.error : ''}</HelpBlock>
    </FormGroup>
  )
}

export default InputGroupField