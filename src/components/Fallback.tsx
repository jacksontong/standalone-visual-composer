import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons'

const Fallback = () => (
  <FontAwesomeIcon
    icon={faSyncAlt}
    spin
    size="lg"
  />
)

export default Fallback