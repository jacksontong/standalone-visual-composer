import React from 'react'
import { Link } from 'react-router-dom'
import { homepage } from '../../../package.json'

interface Props {
  disabled?: boolean
  active?: boolean
  baseUrl: string
  page: number
}
export const Item: React.FC<Props> = ({
  children,
  active = false,
  disabled = false,
  baseUrl,
  page
}) => {
  const url = new URL(baseUrl)
  url.searchParams.set('page', page.toString())

  return (
    <li
      className={(disabled ? "disabled" : "") + (active ? " active" : "")}
    >
      {disabled ? <span>{children}</span> :
        <Link to={`${url.pathname.replace(homepage, "")}${url.search}`}>
          {children}
        </Link>
      }
    </li>
  )
}

export default Item