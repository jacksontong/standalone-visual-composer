import React from 'react'
import Item from './Item'

interface Props {
  currentPage: number
  pageCount: number
  hideOnSinglePage?: boolean
  maxButtonCount?: number
  baseUrl: string
}

const Pagination: React.FC<Props> = ({
  currentPage,
  pageCount,
  hideOnSinglePage = true,
  maxButtonCount = 10,
  baseUrl
}) => {
  if ((!pageCount || pageCount < 2) && hideOnSinglePage) {
    return <span />
  }

  const getPageRange = () => {
    let beginPage = Math.max(0, currentPage - maxButtonCount / 2)
    let endPage = beginPage + maxButtonCount - 1
    if (endPage >= pageCount) {
      endPage = pageCount - 1
      beginPage = Math.max(0, endPage - maxButtonCount + 1)
    }

    return [beginPage, endPage]
  }

  const renderInternalPages = () => {
    const [beginPage, endPage] = getPageRange()
    const pages = []
    for (let i = beginPage; i <= endPage; i++) {
      pages.push(
        <Item
          active={i === currentPage}
          key={i}
          baseUrl={baseUrl}
          page={i + 1}
        >
          {i + 1}
        </Item>
      )
    }
    return pages
  }

  return (
    <ul className="pagination">
      <Item
        disabled={currentPage <= 0}
        baseUrl={baseUrl}
        page={currentPage}
      >
        &laquo;
      </Item>

      {renderInternalPages()}

      <Item
        disabled={currentPage >= pageCount - 1}
        baseUrl={baseUrl}
        page={currentPage + 2}
      >
        &raquo;
      </Item>
    </ul>
  )
}

export default Pagination