import React, { Suspense } from 'react'
import {
  BrowserRouter as Router,
} from "react-router-dom"
import { createStore } from "redux-dynamic-modules"
import { getSagaExtension } from "redux-dynamic-modules-saga"
import { Provider } from "react-redux"
import Routes from './Routes'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './App.css'
import Fallback from './components/Fallback'

const store = createStore({ extensions: [getSagaExtension()] })

toast.configure({
  draggable: false,
  position: "bottom-right"
})

const App = () => (
  <Provider store={store}>
    <Router basename="standalone">
      <Suspense fallback={<Fallback />}>
        <Routes />
      </Suspense>
    </Router>
  </Provider>
)

export default App
