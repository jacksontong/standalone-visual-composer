----
-- User configuration file for lsyncd.
--
-- Simple example for default rsync, but executing moves through on the target.
--
-- For more examples, see /usr/share/doc/lsyncd*/examples/
--
settings {
  logfile = "/var/log/lsyncd/lsyncd.log",
  statusFile = "/var/log/lsyncd/lsyncd.status"
}

sync {
  default.rsync,
  source = "/home/jackson/Documents/node/proposal-standalone/build",
  target = "/home/jackson/Documents/php/pvsell/public_html/standalone",
  exclude = { 'index.html' },
  rsync = {
    archive = true,
    compress = true,
    perms = true,
    acls = true,
    owner = true,
  }
}
